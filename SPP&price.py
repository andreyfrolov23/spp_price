import time
import httplib2
import pandas as pd
# import df2gspread as d2g
import requests
import json
import configparser
import telebot

from googleapiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials


config = configparser.ConfigParser()
config.read("/etc/bot/andrew/spp_price/config")

TOKEN = config['telegram']['TOKEN']
bot = telebot.TeleBot(TOKEN)
SPP = config['telegram']['SPP']  # Daniil spp
price = config['telegram']['price']  # Daniil price
SPP_vit = config['telegram']['SPP_vit']  # Vitaliy spp
price_vit = config['telegram']['price_vit']  # Vitaliy price

sheet_base = config['sheet']['guys']
sheet_vitaliy = config['sheet']['vitaliy']
sheets = [sheet_base, sheet_vitaliy]


def sending(sheet_name, string_warn_name, chat):
    try:
        if (sheet_name == sheet_base) and (chat == 'price'):
            bot.send_message(chat_id=price, text=string_warn_name, parse_mode='Markdown')
        elif (sheet_name == sheet_base) and (chat == 'spp'):
            bot.send_message(chat_id=SPP, text=string_warn_name, parse_mode='Markdown')
        elif (sheet_name == sheet_vitaliy) and (chat == 'spp'):
            bot.send_message(chat_id=SPP_vit, text=string_warn_name, parse_mode='Markdown')
        else:
            bot.send_message(chat_id=price_vit, text=string_warn_name, parse_mode='Markdown')
    except:
        print('bad request to telegram')


def checking():
    for sheet in sheets:
        scopes = ['https://www.googleapis.com/auth/spreadsheets']
        creds_service = ServiceAccountCredentials.from_json_keyfile_name\
            ('/etc/bot/andrew/spp_price/credits_guys.json', scopes).authorize(httplib2.Http())
        my_table = build('sheets', 'v4', http=creds_service)
        resp = my_table.spreadsheets().values().batchGet(spreadsheetId=sheet, ranges="История продаж!A1:F").execute()

        df_base = pd.DataFrame(resp['valueRanges'][0]['values'])
        df_base.columns = df_base.loc[0, :]
        df_base.drop(labels=0, axis=0, inplace=True)

        our_sales = my_table.spreadsheets().values().batchGet(spreadsheetId=sheet, ranges="Продажи наши СПП и цена").execute()
        df_our_sales = pd.DataFrame(our_sales['valueRanges'][0]['values'])
        df_our_sales.columns = df_our_sales.loc[0, :]
        df_our_sales.drop(labels=0, axis=0, inplace=True)

        #print(df_base)
        #print(df_our_sales)

        for art in df_our_sales['Артикул']:
            ident = df_our_sales.loc[(df_our_sales['Артикул'] == art), ['Идентификатор']].values[0][0]
            main_url = "https://www.wildberries.ru/catalog/{ART}/detail.aspx".format(ART=art)
            try:
                old_spp = int(df_our_sales.loc[(df_our_sales['Артикул'] == art), ['Размер СПП']].values[0][0])
            except:
                old_spp = int(df_base.loc[(df_base['Артикул'] == art), ['Размер СПП']].values[0][0])
            new_spp = int(df_base.loc[(df_base['Артикул'] == art), ['Размер СПП']].values[0][0])
            try:
                old_price = int(df_our_sales.loc[(df_our_sales['Артикул'] == art), ['Текущая цена']].values[0][0])
            except:
                old_price = int(df_base.loc[(df_base['Артикул'] == art), ['Текущая цена']].values[0][0])
            new_price = int(df_base.loc[(df_base['Артикул'] == art), ['Текущая цена']].values[0][0])
            if old_spp == 0 and new_spp != 0:
                string_warn = 'СПП ' + '[{ident}]({url})'.format(ident=ident, url=main_url) + ' изменилось с 0 на {size}'.format(size=new_spp)
                sending(sheet_name=sheet, string_warn_name=string_warn, chat='spp')
            elif old_spp != 0 and new_spp == 0:
                string_warn = 'СПП ' + '[{ident}]({url})'.format(ident=ident, url=main_url) + ' изменилось с {size} на 0'.format(size=old_spp)
                sending(sheet_name=sheet, string_warn_name=string_warn, chat='spp')
            df_our_sales.loc[(df_our_sales['Артикул'] == art), ['Размер СПП']] = new_spp
            if old_price != new_price:
                sales_qty = df_base.loc[(df_base['Артикул'] == art), ['Продажи за день']].values[0][0]
                string_warn = 'Цена ' + '[{ident}]({url})'.format(ident=ident, url=main_url) + ' изменилась с {old} на {new}'.format(old=old_price, new=new_price) + '\n' + 'Продаж было: {sales_qty}'.format(sales_qty=sales_qty)
                sending(sheet_name=sheet, string_warn_name=string_warn, chat='price')

            df_our_sales.loc[(df_our_sales['Артикул'] == art), ['Текущая цена']] = new_price

        body = {
                    'data': [
                        {
                            'range': 'Продажи наши СПП и цена',
                            'majorDimension': 'COLUMNS',
                            'values': [df_our_sales.columns.values.tolist()]
                        },
                        {
                            'range' : 'Продажи наши СПП и ценаы',
                            'majorDimension': 'ROWS',
                            'values': df_our_sales.values.tolist()
                        }
                    ]
                }

        request = my_table.spreadsheets().values().update(spreadsheetId=sheet,
                                                          valueInputOption='USER_ENTERED',
                                                          range='Продажи наши СПП и цена',
                                                          body={'values': [df_our_sales.columns.tolist()] + df_our_sales.values.tolist()}).execute()


while True:
    checking()
    time.sleep(5)
