import time
import httplib2
import pandas as pd
# import df2gspread as d2g
import requests
import json
import configparser
from datetime import date, datetime

from googleapiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials

today = datetime.now().date()
# today = '2023-07-05'

config = configparser.ConfigParser()
config.read("/etc/bot/andrew/spp_price/config")

sheet_vitaliy = config['sheet']['vitaliy']

scopes = ['https://www.googleapis.com/auth/spreadsheets']
creds_service = ServiceAccountCredentials.from_json_keyfile_name\
    ('/etc/bot/andrew/spp_price/credits_guys.json', scopes).authorize(httplib2.Http())
my_table = build('sheets', 'v4', http=creds_service)


def size_qty(art_list):
        size = art_list['origName']
        qty = 0
        size_cm = art_list['name']
        for stock in art_list['stocks']:
            qty += stock['qty']
        return size, size_cm, qty


def new_day(date):
    table = my_table.spreadsheets().values().batchGet(spreadsheetId=sheet_vitaliy, ranges="Размеры").execute()
    df_table = pd.DataFrame(table['valueRanges'][0]['values'])
    df_table.columns = df_table.loc[0, :]
    df_table.drop(labels=0, axis=0, inplace=True)
    new_line = df_table['Продажи за день'].tolist()
    # today_date = date.today()
    df_table[str(date)] = new_line
    df_table['Продажи за вчера'] = new_line
    df_table['Продажи за день'] = 0
    body = {
        'data': [
            {
                'range': 'Размеры',
                'majorDimension': 'COLUMNS',
                'values': [df_table.columns.values.tolist()]
            },
            {
                'range': 'Размеры',
                'majorDimension': 'ROWS',
                'values': df_table.values.tolist()
            }
        ]
    }

    request = my_table.spreadsheets().values().update(spreadsheetId=sheet_vitaliy,
                                                      valueInputOption='USER_ENTERED',
                                                      range='Размеры',
                                                      body={
                                                          'values': [df_table.columns.tolist()] + df_table.values.tolist()}).execute()


while True:
    table_sizes = my_table.spreadsheets().values().batchGet(spreadsheetId=sheet_vitaliy, ranges="Размеры!A1:F").execute()
    df_table_sizes = pd.DataFrame(table_sizes['valueRanges'][0]['values'])
    df_table_sizes.columns = df_table_sizes.loc[0, :]
    df_table_sizes.drop(labels=0, axis=0, inplace=True)

    # print(today)
    arts = []
    for art in df_table_sizes['Артикул']:
        if art not in arts:
            arts.append(art)

    for art in arts:
        url = "https://card.wb.ru/cards/detail?spp=0&regions=80,64,38,4,83,33,68,70,69,30,86,75,40,1,22,66,31,48,71&" \
              "pricemarginCoeff=1.0&reg=0&appType=1&emp=0&locale=ru&lang=ru&curr=rub&couponsGeo=" \
              "12,3,18,15,21&dest=-1257786&nm={art}".format(art=art)  # 5163995;6170054;31211424"

        res = requests.get(url, headers={
            "accept": "*/*",
            "accept-language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
            "sec-ch-ua": "\"Chromium\";v=\"110\", \"Not A(Brand\";v=\"24\", \"Google Chrome\";v=\"110\"",
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": "\"Windows\"",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "cross-site"
        })

        res_json = json.loads(res.text)
        for i_list in res_json['data']['products'][0]['sizes']:
            size, size_cm, qty = size_qty(i_list)
            # print(size, qty)
            if df_table_sizes.loc[(df_table_sizes['Артикул'] == art) & (df_table_sizes['Размер'] == size)].empty:
                ident = df_table_sizes.loc[(df_table_sizes['Артикул'] == art), ['Идентификатор']].values[0][0]
                new_row = {'Артикул': art, 'Идентификатор': ident, 'Размер': size, 'Размер см': size_cm,
                           'Остаток': qty, 'Продажи за день': 0}
                df1 = pd.DataFrame([new_row])
                df_table_sizes = pd.concat([df_table_sizes, df1], ignore_index=True)
            else:
                # print(size, qty)
                delta = int(df_table_sizes.loc[((df_table_sizes['Артикул'] == art) & (df_table_sizes['Размер'] == size)),
                                         ['Остаток']].values[0][0]) - int(qty)
                # print(delta)
                if delta > 0:
                    day_sales = int(df_table_sizes.loc[((df_table_sizes['Артикул'] == art) & (df_table_sizes['Размер'] == size)),
                                           ['Продажи за день']].values[0][0]) + delta
                    df_table_sizes.loc[(df_table_sizes['Артикул'] == art) & (df_table_sizes['Размер'] == size),
                                       ['Продажи за день']] = day_sales
                    df_table_sizes.loc[(df_table_sizes['Артикул'] == art) & (df_table_sizes['Размер'] == size),
                                       ['Остаток']] = qty
                else:
                    df_table_sizes.loc[(df_table_sizes['Артикул'] == art) & (df_table_sizes['Размер'] == size),
                                       ['Остаток']] = qty

    # print(df_table_sizes)


    body = {
                    'data': [
                        {
                            'range': 'Размеры!A1:F',
                            'majorDimension': 'COLUMNS',
                            'values': [df_table_sizes.columns.values.tolist()]
                        },
                        {
                            'range' : 'Размеры!A1:F',
                            'majorDimension': 'ROWS',
                            'values': df_table_sizes.values.tolist()
                        }
                    ]
                }

    request = my_table.spreadsheets().values().update(spreadsheetId=sheet_vitaliy,
                                                          valueInputOption='USER_ENTERED',
                                                          range='Размеры!A1:F',
                                                          body={'values': [df_table_sizes.columns.tolist()] + df_table_sizes.values.tolist()}).execute()
    if today != datetime.now().date():
        new_day(today)
        today = datetime.now().date()
    time.sleep(300)
